This repo is my personal build of dmenu. The primary features of this build are the Nord colors and the centering of the window.

dmenu - dynamic menu
====================
dmenu is an efficient dynamic menu for X.


Requirements
------------
In order to build dmenu you need the Xlib header files.


Installation
------------
Edit config.mk to match your local setup. This build of st is installed into the $HOME/.local namespace by default. You may want to change this to /usr/local, which can be done easily through the following command, or by manually editing config.mk and changing the PREFIX variable.

    patch < patches/usr-local.diff

Afterwards enter the following command to build and install dmenu
(if necessary as root):

    make clean install


Running dmenu
-------------
See the man page for details.
